# Demonstrates the lifecyle of a GitLab pipeline with multiple branches

This is an example showing the values used in a GitLab pipeline for a git repository with multiple branches.

## Branches

* main 
* preprod
* prod

## Scenarios

* [main] changes and direct push
* [preprod] temporary branch for merge of main and changes, then Merge Request->preprod
* [prod] temporary branch for merge from main and changes, then Merge Request->prod
* [prod] push of tag


## Create MR on new branch

```
cur_branch=$(git branch --show-current)
new_branch="new-branch-from-${cur_branch}-$RANDOM"
git checkout -b $new_branch
git commit -a -m "MR request targeted for $cur_branch"
git push -u origin $new_branch
```

## Delete MR branch after successful merge

```
git branch -d $new_branch; git push -d origin $new_branch
```

# Creating tag that invokes GitLab pipeline

```
newtag=v1.0.1
git commit -a -m "changes for new tag $newtag" && git push -o ci.skip
git tag $newtag && git push origin $newtag
```

# Deleting tag

```
# delete local tag, then remote
todel=v1.0.1
git tag -d $todel && git push -d origin $todel
```




