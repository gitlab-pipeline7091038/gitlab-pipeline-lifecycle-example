#!/bin/bash
#
# https://stackoverflow.com/questions/11405134/identifying-a-tag-belongs-to-which-branch-in-git
# https://git-scm.com/docs/git-branch

for tag in $(git tag)
do
    commit=$(git rev-parse $tag~0)
    echo "$tag: $(git rev-parse $commit~0) | branch: $(git branch -r --contains $commit)" | grep branch
done

